package org.example;

class CallingCard {

    private int remainingMinutes;
    private int dollars;
    private int centsPerMinute;

    public CallingCard() {

    }

    public CallingCard(int centsPerMinute) {
        this.centsPerMinute = centsPerMinute;
    }

    public int getRemainingMinutes() {
        if (remainingMinutes <= 0) {
            remainingMinutes = 0;
        } else {
            remainingMinutes = remainingMinutes;
        }
        return remainingMinutes;
    }

    public void setRemainingMinutes(int remainingMinutes) {
        this.remainingMinutes = remainingMinutes;
    }

    public void addDollars(int dollars) {
        remainingMinutes = (dollars * 100) / centsPerMinute;

    }

    public void useMinutes(int minutes) {
        remainingMinutes -= minutes;
    }


}
