package org.example;

import java.util.ArrayList;

class CellPhone {
    private ArrayList<String> callHistory = new ArrayList<>();
    CallingCard card;

    public boolean isTalking = false;
    private String phoneNumber;
    private int ticks;
    private boolean outOfMinutes = false;

    public CellPhone(CallingCard card) {
        this.card = card;

    }

    public boolean isTalking(){
        return isTalking;
    }


    public void call(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        isTalking = true;

    }

    public void endCall() {
        isTalking = false;
        if (outOfMinutes != false){
            if (ticks > 1) {
                callHistory.add(phoneNumber + "(" + ticks + " minutes)(This call was cut off)");
            }else if (ticks <= 1){ callHistory.add(phoneNumber + "(" + ticks + " minute)(This call was cut off)");}
        } else{
            if (ticks > 1) {
                callHistory.add(phoneNumber + "(" + ticks + " minutes)");
            }else if (ticks <= 1){ callHistory.add(phoneNumber + "(" + ticks + " minute)");}
        }



        ticks = 0;
    }

    public void tick() {
        if(card.getRemainingMinutes() >= 1){
            card.useMinutes(1);
            ticks++;
        } else {
            outOfMinutes = true;
            endCall();}



    }

    public String getHistory() {
        return String.valueOf(callHistory);
    }


}
