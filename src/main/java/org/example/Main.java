package org.example;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        CallingCard card1 = new CallingCard(20);
        CellPhone cellPhone1 = new CellPhone(card1);
        card1.addDollars(5);
        card1.getRemainingMinutes();
        System.out.println(card1.getRemainingMinutes());

        cellPhone1.call("210-531-8722");
        cellPhone1.tick();
        cellPhone1.tick();
        cellPhone1.tick();
        cellPhone1.endCall();
        cellPhone1.getHistory();
        card1.getRemainingMinutes();
        System.out.println(card1.getRemainingMinutes());

        cellPhone1.call("210-444-4444");
        cellPhone1.tick();
        cellPhone1.tick();
        cellPhone1.endCall();
        cellPhone1.getHistory();
        card1.getRemainingMinutes();
        System.out.println(card1.getRemainingMinutes());

        card1.useMinutes(18);
        System.out.println(card1.getRemainingMinutes());

        cellPhone1.call("222-5555");
        cellPhone1.tick();
        System.out.println(cellPhone1.isTalking);
        cellPhone1.endCall();

        cellPhone1.getHistory();
        System.out.println(cellPhone1.getHistory());
        System.out.println(card1.getRemainingMinutes());

        cellPhone1.call("123-4567");
        cellPhone1.tick();
        cellPhone1.tick();
        cellPhone1.endCall();

        cellPhone1.getHistory();
        System.out.println(cellPhone1.getHistory());





    }
}


